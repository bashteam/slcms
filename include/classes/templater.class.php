<?php

	#slCms: created in version 0.1
	#by slDevelop/bashteam
	
	function htmlout($style,$template,$data) {
		require_once "include/classes/rain.tpl.class.php";
		raintpl::configure("base_url", null );
		raintpl::configure("tpl_dir", SLROOT."template/".$style."/" );
		raintpl::configure("cache_dir", SLROOT."tmp/" );
		$tpl->assign($data);
		$tpl->draw($template);	
	}
	
?>
